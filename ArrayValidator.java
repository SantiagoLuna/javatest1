public class ArrayValidator{

    public static boolean validate(int[] validNums, int[] input){
        boolean isSame = false;
        int counter = 0;

        for(int i=0; i<input.length; i++){
            for(int n=0; n<validNums.length; n++){
                if(input[i] == validNums[n]){
                    counter++;
                    isSame = true;
                    break;
                }
                isSame = false;
            }
        }
        if(counter == input.length){
            return isSame = true;
        }

        return isSame;
    }

    
}

