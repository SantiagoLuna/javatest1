import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestArrayValidator {
    
    @Test
    public void testValid(){
        int [] input = {1,2,3};
        int[] valid = {1,2,3};
        assertEquals(true, ArrayValidator.validate(valid, input));
    }

    @Test
    public void testNotValid(){
        int [] input = {0,0,0};
        int[] valid = {1,2,3};
        assertEquals(false, ArrayValidator.validate(valid, input));
    }

    @Test
    public void testOneInput(){
        int [] input = {1};
        int[] valid = {1,2,3};
        assertEquals(true, ArrayValidator.validate(valid, input));
    }
    @Test
    public void testAllSame(){
        int [] input = {1,1,1};
        int[] valid = {1,2,3};
        assertEquals(true, ArrayValidator.validate(valid, input));
    }
    @Test
    public void testOneRight(){
        int [] input = {1,0,0};
        int[] valid = {1,2,3};
        assertEquals(false, ArrayValidator.validate(valid, input));
    }
    @Test
    public void testLastRight(){
        int [] input = {0,0,1};
        int[] valid = {1,2,3};
        assertEquals(false, ArrayValidator.validate(valid, input));
    }
    @Test
    public void testFirstRight(){
        int [] input = {1,0,0};
        int[] valid = {1,2,3};
        assertEquals(false, ArrayValidator.validate(valid, input));
    }
    @Test
    public void testBigInput(){
        int [] input = {1,1,1,1,1};
        int[] valid = {1,2,3};
        assertEquals(true, ArrayValidator.validate(valid, input));
    }

}
